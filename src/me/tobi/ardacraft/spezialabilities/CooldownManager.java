package me.tobi.ardacraft.spezialabilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CooldownManager {

	private HashMap<SpezialAbility, List<Player>> cooldowns = new HashMap<>();
	
	public void addCooldown(SpezialAbility sa, Player p) {
		List<Player> sac = cooldowns.get(sa);
		if(sac == null) {
			sac = new ArrayList<Player>();
		}
		sac.add(p);
		cooldowns.put(sa, sac);
	}
	
	public boolean hasCooldown(Player p, SpezialAbility sa) {
		try {
			if(cooldowns.get(sa).contains(p)) {
				return true;
			}else {
				return false;
			}
		}catch(NullPointerException ex) {
			return false;			
		}
	}
	
	public void removeCooldown(SpezialAbility sa, Player p) {
		List<Player> sac = cooldowns.get(sa);
		sac.remove(p);
		cooldowns.put(sa, sac);
	}

	public void runScheduler(final SpezialAbility sa, final Player p, int cd) {
		addCooldown(sa, p);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {
		@Override
			public void run() {
				removeCooldown(sa, p);
			}				
		}, cd);
	}
	
}
