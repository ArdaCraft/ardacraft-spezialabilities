package me.tobi.ardacraft.spezialabilities;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils.Attitude;

import net.minecraft.server.v1_10_R1.EntityArrow;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.WitherSkull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public enum SpezialAbility {
	
	
	LAUNCH_ARROW {
		@Override
		public int getDefaultCooldown() {
			return 2*20;
		}
		@Override
		public Object getDefaultObject() {
			return false;
		}
		/**
		 * @param o Boolean, collectable arrow?
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Arrow ar = p.launchProjectile(Arrow.class);
			ar.setShooter(p);
			ar.setBounce(true);
			if((Boolean)o == false) {
				try {
					Method getHandleMethod = ar.getClass().getMethod("getHandle");
					Object handle = getHandleMethod.invoke(ar);
					Field fromPlayerField = handle.getClass().getField("fromPlayer");
					fromPlayerField.set(handle, EntityArrow.PickupStatus.DISALLOWED);
				} catch (Throwable ex) {
					ex.printStackTrace(); 
					return -1;
				}
			}
			ar.setVelocity(p.getLocation().getDirection().multiply(2));
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, LAUNCH_ARROW_BURNING {
		@Override
		public int getDefaultCooldown() {
			return 2*20;
		}
		@Override
		public Object getDefaultObject() {
			return false;
		}
		/**
		 * @param o Boolean, collectable arrow?
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Arrow ar = p.launchProjectile(Arrow.class);
			ar.setShooter(p);
			ar.setBounce(true);
			ar.setFireTicks(Integer.MAX_VALUE);
			if((Boolean)o == false) {
				try {
					Method getHandleMethod = ar.getClass().getMethod("getHandle");
					Object handle = getHandleMethod.invoke(ar);
					Field fromPlayerField = handle.getClass().getField("fromPlayer");
					fromPlayerField.set(handle, EntityArrow.PickupStatus.DISALLOWED);
				} catch (Throwable ex) {
					ex.printStackTrace(); 
					return -1;
				}
			}
			ar.setVelocity(p.getLocation().getDirection().multiply(2));
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, LAUNCH_WITHERSKULL {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			WitherSkull ws = p.launchProjectile(WitherSkull.class);
			ws.setShooter(p);
			ws.setBounce(true);
			ws.setVelocity(p.getLocation().getDirection().multiply(2));
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, LAUNCH_FIREBALL {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Location loc = p.getEyeLocation().toVector().add(p.getLocation().getDirection().multiply(2)).toLocation(p.getWorld(), p.getLocation().getYaw(), p.getLocation().getPitch());
			Fireball fireball = p.getWorld().spawn(loc, Fireball.class);
			fireball.setShooter(p);
			fireball.setBounce(true);
			fireball.setIsIncendiary(false);
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, STRIKE_LIGHTNINGS_DELAYED {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			@SuppressWarnings("deprecation")
			final Location loc = p.getTargetBlock(new HashSet<Byte>(), 5).getLocation();
			Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					for(int i = 0; i < 10; i++) {
						loc.getWorld().strikeLightning(loc);
					}
				}
				
			}, 20);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, RING {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		/**
		 * @param o Integer tier
		 */
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			int tier = (int)o;
			if(tier == 0) { //cd = 1min
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 600, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 600, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 600, 0));
			}else if(tier == 1) { //cd = 1,5 min
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1200, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1200, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 200, 0));
			}
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, LAUNCH_THREE_FIREBALLS {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(final Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			fballs = 0;
			Bukkit.getScheduler().scheduleSyncRepeatingTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					if(fballs >= 3) {
						try {
							this.finalize();
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}else {
						SpezialAbility.LAUNCH_FIREBALL.execute(p, null, 0);
						fballs++;
					}
				}
				
			}, 0, 30);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, DOLCH_ABILITY {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			p.setFoodLevel(20);
			p.setHealth(20);
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, KAMPFAXT_ABILITY {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown){
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 0));
			p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 200, 0));
			p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 600, 0));
			Random rnd = new Random();
			if(rnd.nextInt(100) < 6) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 5));
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, TOGGLE_HIDE {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			if(p.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
				p.removePotionEffect(PotionEffectType.INVISIBILITY);
			}else {
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
			}
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
	}, REMOVE_ITEM {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}
		@Override
		public Object getDefaultObject() {
			return new ItemStack(Material.AIR);
		}
		/**
		 * @param o instanceof ItemStack zu entfernendes Item
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			ItemStack i = (ItemStack)o; //TODO Überprüfen
			if(i.getAmount() == 1) {
				p.getInventory().remove(i);
			}else {
				p.getInventory().remove(i);
				ItemStack add = i;
				add.setAmount(i.getAmount()-1);
				p.getInventory().addItem(add);
			}
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, CREATE_EXPLOSION {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}
		@Override
		public Object getDefaultObject() {
			return 5;
		}
		/**
		 * @param o intanceof Integer; Explosionsgröße
		 */
		@Override
		public int execute(final Player p, final Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			final Location loc = p.getLocation();
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {
				@Override
				public void run() {
					p.getLocation().getWorld().createExplosion(loc, (Integer)o, true);
					return;
				}				
			}, 2*20L);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, CREATE_TREE {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().generateTree(p.getLocation(), TreeType.TREE);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, STRIKE_LIGHTNING {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().strikeLightning(p.getLocation());
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_SLIME {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SLIME);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_SQUID {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SQUID);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_CREEPER{
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.CREEPER);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, FEED_HALF{
		@Override
		public int getDefaultCooldown() {
			return 11*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.setFoodLevel(p.getFoodLevel() + 10);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, FEED_FULL {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.setFoodLevel(p.getFoodLevel() + 20);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, APPLY_SPEED {
		@Override
		public int getDefaultCooldown() {
			return 15*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int) (10*20L), 2));
						
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, APPLY_STRENGTH {
		@Override
		public int getDefaultCooldown() {
			return 15*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, (int) (10*20L), 2));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, APPLY_JUMP {
		@Override
		public int getDefaultCooldown() {
			return 15*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}

			p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, (int) (10*20L), 2));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, THROW_ENDERPEARL {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			EnderPearl ep = p.launchProjectile(EnderPearl.class);
			ep.setShooter(p);
			ep.setVelocity(p.getLocation().getDirection().multiply(2));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, THROW_EGG {
		@Override
		public int getDefaultCooldown() {
			return 1*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			Egg e = p.launchProjectile(Egg.class);
			e.setShooter(p);
			e.setBounce(true);
			e.setVelocity(p.getLocation().getDirection().multiply(2));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, THROW_SNOWBALL {
		@Override
		public int getDefaultCooldown() {
			return 1*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Snowball sb = p.launchProjectile(Snowball.class);
			sb.setShooter(p);
			sb.setBounce(true);
			sb.setVelocity(p.getLocation().getDirection().multiply(2));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, TELEPORT_VIEW {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@SuppressWarnings("deprecation")
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Location loc = p.getTargetBlock((HashSet<Byte>)null, 100).getLocation();
			if(loc == null) {
				return -1;
			}
			p.teleport(loc);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, TELEPORT_NEAR_RANDOM {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}
		
		/**
		 * @param o Integer radius
		 */
		@Deprecated
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, CREATE_DIRTWALL {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			createWall(p, Material.DIRT);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}

		
		
	}, CREATE_FIRECIRCLE {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			createFirecircle(p);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, DAMAGE_ENTITY_NEAREST {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			for(Entity e : p.getWorld().getEntities()) {
				  if(e instanceof LivingEntity) {
					  if(e.getLocation().distance(p.getLocation()) < 20) {
						  ((LivingEntity) e).damage(4);
					  }
				  }
			  }
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, APPLY_INVISIBLITY {
		@Override
		public int getDefaultCooldown() {
			return 20*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}

			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (int) (15*20L), 1));
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, CREATE_SPIDERWEBWALL {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			createWall(p, Material.WEB);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_TNT_PRIMED{
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getWorld().spawnEntity(p.getLocation(), EntityType.PRIMED_TNT);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SHOW_PLAYERS_NEAR{
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}
		
		/**
		 * @param o Integer radius
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			boolean found = false;
			for(Entity e : p.getWorld().getEntities()) {
				if(e instanceof Player) {
					if(((Player)e).getName() != p.getName()) {
						String cc = Rasse.get((Player)e).getAttitude() == Attitude.BAD?"�8":"�a";
						p.sendMessage(cc + e.getName() + " (" + (int)p.getLocation().distance(e.getLocation()) + "): �b" + "X: " + e.getLocation().getX() + ", Y: " + e.getLocation().getY() + ", Z: " + e.getLocation().getZ());
						found = true;
					}
				}
			}
			if(found == false) {
				p.sendMessage("Keine Spieler in der N�he!");
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_ZOMBIE {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, SPAWN_ZOMBIE_MULTIPLE {
		@Override
		public int getDefaultCooldown() {
			return 7*20;
		}
		
		/**
		 * @param o count
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			for(int i = 0;i < (Integer)o;i++) {
				p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE);
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, STRIKE_LIGHTNING_NEAREST_ENTITY {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}
		
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			double dist = Integer.MAX_VALUE;
			Entity target = null;
			for(Entity e : p.getWorld().getEntities()) {
				if(e instanceof LivingEntity) {
					double dista = e.getLocation().distance(p.getLocation());
					if(e != p) {
						if(dista < dist) {
							dist = dista;
							target = e;
						}
					}
				}
			}
			p.getWorld().strikeLightning(target.getLocation());
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, STRIKE_LIGHTNING_NEAR_RANDOM {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}
		
		/**
		 * @param o Integer radius
		 */
		@Deprecated
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, LAUNCH_ARROW_MULTIPLE {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			for (int x = 0; x < 180; x += 36) {
				for (int y = 0; y < 180; y += 36) {
					Arrow arr = p.launchProjectile(Arrow.class);
					arr.setShooter(p);
					arr.setBounce(true);
					Vector velocity = new Vector(Math.sin(x) * 1.8D, 0.3D, Math.cos(y) * 1.8D);
					arr.setVelocity(velocity);
				}
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, HEAL_HALF {
		@Override
		public int getDefaultCooldown() {
			return 6*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			p.setHealth(p.getMaxHealth() / 2);
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, HEAL_FULL {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			

			p.setHealth(p.getMaxHealth());
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, DAMAGE_ENTITIES_NEAR_MULTIPLE {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}
		
		/**
		 * @param o Integer radius
		 */
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			for(Entity e : p.getWorld().getEntities()) {
				if(e instanceof LivingEntity) {
					if(e.getLocation().distance(p.getLocation()) < (Integer)o) {
						((LivingEntity) e).setHealth(0);
					}
				}
			}
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, THROW_TNT_PRIMED {
		@Override
		public int getDefaultCooldown() {
			return 10*20;
		}

		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			Location loc = p.getLocation();
			Entity ptnt = p.getWorld().spawnEntity(loc, EntityType.PRIMED_TNT);
			ptnt.setVelocity(p.getLocation().getDirection().multiply(2));
			
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	}, LAUNCH_ROCKET {
		@Override
		public int getDefaultCooldown() {
			return 5*20;
		}
		@Override
		public int execute(Player p, Object o, int cooldown) {
			if(ArdaCraftSpezialAbilities.getCooldownManager().hasCooldown(p, this)) {
				return 1;
			}
			
			Firework fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
			FireworkMeta fwm = fw.getFireworkMeta();
			
			FireworkEffect effect = FireworkEffect.builder().withColor(Color.RED).flicker(true).build();
			
			fwm.addEffect(effect);
			fwm.setPower(2);
			fw.setFireworkMeta(fwm);     
			
			ArdaCraftSpezialAbilities.getCooldownManager().runScheduler(this, p, cooldown);
			return 0;
		}
		
	};
	
	public abstract int execute(Player p, Object o, int cooldown);
	
	private static void createWall(Player p, Material m) {

		p.getWorld().playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 0, 10);
		
		Location W = new Location(p.getWorld(), p.getLocation().getX() + 3, p.getLocation().getY() - 2, p.getLocation().getZ() + 3);
		Location N = new Location(p.getWorld(), p.getLocation().getX() - 3, p.getLocation().getY() - 2, p.getLocation().getZ() + 3);
		Location E = new Location(p.getWorld(), p.getLocation().getX() - 3, p.getLocation().getY() - 2, p.getLocation().getZ() - 3);
		Location S = new Location(p.getWorld(), p.getLocation().getX() + 3, p.getLocation().getY() - 2, p.getLocation().getZ() - 3);
		
		List<Location> dirtblocks = new ArrayList<Location>();
		
		for(int count1 = 0; count1 < 7; count1++) {
			for(int count2 = 0; count2 < 6; count2++) {
				Location tmp = null;
				if(getCardinalDirection(p) == "N")
				tmp = N.clone().add(0 , count2, -count1);
				if(getCardinalDirection(p) == "W")
				tmp = W.clone().add(-count1, count2, 0);
				if(getCardinalDirection(p) == "E")
				tmp = E.clone().add(count1, count2, 0);
				if(getCardinalDirection(p) == "S")
				tmp = S.clone().add(0, count2, count1);
				if(tmp.getBlock().getType() == Material.AIR) {
					dirtblocks.add(tmp);
					tmp.getBlock().setType(m);
				}
			}
		}		
		
		final List<Location> dirtblocksF = dirtblocks;
		Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {

			@Override
			public void run() {
				for(Location lo : dirtblocksF) {
					lo.getBlock().setType(Material.AIR);
				}
				
			}
			
		}, 300);
	}

	private  static String getCardinalDirection(Player player) {
	    double rotation = (player.getLocation().getYaw() - 90) % 360;
	    if (rotation < 0) {
	        rotation += 360.0;
	    }
	     if (0 <= rotation && rotation < 45) {
	        return "N";
	    } else if (45 <= rotation && rotation < 135) {
	        return "E";
	    } else if (135 <= rotation && rotation < 225) {
	        return "S";
	    } else if (225 <= rotation && rotation < 315) {
	        return "W";
	    } else if (315 <= rotation && rotation < 360) {
	        return "N";
	    } else {
	        return null;
	    }
	}
	
	private static void createFirecircle(final Player p) {
		final Location grundloc = p.getLocation().clone();
		final Location locs1[] = new Location[9];
		
		locs1[1] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ());
		locs1[2] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ() - 1);
		locs1[3] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ() + 1);
		locs1[4] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ());
		locs1[5] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ() - 1);
		locs1[6] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ() + 1);
		locs1[7] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() + 1);
		locs1[8] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() - 1);
		
		int i = 0;
		
		while(i != 8){
			++i;
			
			if(locs1[i].getBlock().getType() == Material.AIR){
				locs1[i].getBlock().setType(Material.FIRE);
			}
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {

			@Override
			public void run() {
				int i = 0;
				while(i != 8){
					++i;
					
					if(locs1[i].getBlock().getType() == Material.FIRE){
						locs1[i].getBlock().setType(Material.AIR);
					}
				}
				
				final Location locs2[] = new Location[17];
				
				locs2[1] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ());
				locs2[2] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() - 1);
				locs2[3] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() - 2);
				locs2[4] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() + 1);
				locs2[5] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() + 2);
				locs2[6] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ());
				locs2[7] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() - 1);
				locs2[8] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() - 2);
				locs2[9] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() + 1);
				locs2[10] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() + 2);
				locs2[11] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() + 2);
				locs2[12] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() - 2);
				locs2[13] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ() + 2);
				locs2[14] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() + 2);
				locs2[15] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ() - 2);
				locs2[16] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() - 2);
				
				i = 0;
				
				while(i != 16){
					++i;
					
					if(locs2[i].getBlock().getType() == Material.AIR){
						locs2[i].getBlock().setType(Material.FIRE);
					}
				}
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {

					@Override
					public void run() {
						int i = 0;
						while(i != 16){
							++i;
							
							if(locs2[i].getBlock().getType() == Material.FIRE){
								locs2[i].getBlock().setType(Material.AIR);
							}
						}
						
						final Location loc3[] = new Location[33];
						
						loc3[1] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() - 1);
						loc3[2] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() - 2);
						loc3[3] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() - 3);
						loc3[4] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() + 1);
						loc3[5] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() + 2);
						loc3[6] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ() + 3);
						loc3[7] = new Location(p.getWorld(), grundloc.getX() + 3, grundloc.getY(), grundloc.getZ());
						loc3[8] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() - 1);
						loc3[9] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() - 2);
						loc3[10] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() - 3);
						loc3[11] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() + 1);
						loc3[12] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() + 2);
						loc3[13] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ() + 3);
						loc3[14] = new Location(p.getWorld(), grundloc.getX() - 3, grundloc.getY(), grundloc.getZ());
						loc3[15] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ() - 3);
						loc3[16] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() - 3);
						loc3[17] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ() - 3);
						loc3[18] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() - 3);
						loc3[19] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() - 3);
						loc3[20] = new Location(p.getWorld(), grundloc.getX() + 1, grundloc.getY(), grundloc.getZ() + 3);
						loc3[21] = new Location(p.getWorld(), grundloc.getX() + 2, grundloc.getY(), grundloc.getZ() + 3);
						loc3[22] = new Location(p.getWorld(), grundloc.getX() - 1, grundloc.getY(), grundloc.getZ() + 3);
						loc3[23] = new Location(p.getWorld(), grundloc.getX() - 2, grundloc.getY(), grundloc.getZ() + 3);
						loc3[24] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() + 3);
						loc3[25] = new Location(p.getWorld(), grundloc.getX(), grundloc.getY(), grundloc.getZ() + 3);
						
						i = 1;
						
						while(i != 24){
							++i;
							
							if(loc3[i].getBlock().getType() == Material.AIR){
								loc3[i].getBlock().setType(Material.FIRE);
							}
						}
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialAbilities.getPlugin(), new Runnable() {

							@Override
							public void run() {
								int i = 0;
								while(i != 24){
									++i;
									if(loc3[i].getBlock().getType() == Material.FIRE){
										loc3[i].getBlock().setType(Material.AIR);
									}
								}
							}
						}, 1*20L);
					}
				}, 1*20L);
			}
		}, 1*20L);
	}
	
	private static int fballs;
	
	public int getDefaultCooldown() {
		return -1;
	}
	
	public Object getDefaultObject() {
		return null;
	}
	
}
